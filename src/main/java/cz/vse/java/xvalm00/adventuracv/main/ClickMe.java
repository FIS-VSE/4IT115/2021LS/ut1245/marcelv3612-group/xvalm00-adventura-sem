package cz.vse.java.xvalm00.adventuracv.main;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class ClickMe extends Application {

    private Button btn;
    private final String clickMeText  = "Click me please!";

    public static void main(String[] args) {
        System.out.println("Launching JavaFX application.");
        launch(args);
        System.out.println("Finished.");
    }

    @Override
    public void start(Stage primaryStage) {
        // Create the Button
        btn = new Button();
        btn.setText(clickMeText);
        btn.setOnAction(event -> zmacknutoTlacitko());

        // Add the button to a layout pane
        BorderPane pane = new BorderPane();
        pane.setCenter(btn);
        // Add the layout pane to a scene
        Scene scene = new Scene(pane, 300, 250);

        // Finalize and show the stage
        primaryStage.setScene(scene);
        primaryStage.setTitle("The Click Me App");
        primaryStage.show();
    }

    private void zmacknutoTlacitko() {
        if (btn.getText().equals(clickMeText)){
            btn.setText("I've been clicked!");
        } else {
            btn.setText(clickMeText);
        }
    }

}
